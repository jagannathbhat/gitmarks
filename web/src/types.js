export const ALERT_ADD = 'ALERT_ADD'
export const ALERT_NEXT = 'ALERT_NEXT'
export const ALERT_REMOVE = 'ALERT_REMOVE'

export const REPO_ADD = 'REPO_ADD'
export const REPO_INIT = 'REPO_INIT'
export const REPO_REMOVE = 'REPO_REMOVE'
