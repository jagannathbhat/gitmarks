import React from 'react'

import { useStyles } from '../../theme'

const AppTitle = () => {
	const classes = useStyles()

	return (
		<>
			<div className={classes.headerTitle}>GitMarks</div>
			<div className={classes.whiteSpace}></div>
		</>
	)
}

export default AppTitle
