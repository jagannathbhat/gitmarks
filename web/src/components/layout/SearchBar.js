import React from 'react'
import SearchIcon from '@material-ui/icons/Search'
import { Button, fade, InputBase, makeStyles, Paper } from '@material-ui/core'

const SearchBar = ({ onChange, suggestions, value, withIcon = false }) => {
	const localClasses = makeStyles(theme => ({
		inputInput: {
			padding: theme.spacing(1, 1, 1, 0),
			paddingLeft: withIcon
				? `calc(1em + ${theme.spacing(4)}px)`
				: theme.spacing(1),
			transition: theme.transitions.create('width'),
			width: '100%',
			[theme.breakpoints.up('sm')]: {
				width: '12ch',
				'&:focus': {
					width: '20ch',
				},
			},
		},
		inputRoot: { color: 'inherit' },
		search: {
			borderRadius: theme.shape.borderRadius,
			backgroundColor: fade(theme.palette.common.white, 0.15),
			display: 'inline-block',
			marginLeft: 0,
			position: 'relative',
			width: '100%',
			'&:hover': {
				backgroundColor: fade(theme.palette.common.white, 0.25),
			},
			[theme.breakpoints.down('sm')]: { maxWidth: '74%' },
			[theme.breakpoints.up('sm')]: {
				marginLeft: theme.spacing(1),
				width: 'auto',
			},
		},
		searchIcon: {
			alignItems: 'center',
			display: 'flex',
			height: '100%',
			justifyContent: 'center',
			padding: theme.spacing(0, 2),
			pointerEvents: 'none',
			position: 'absolute',
		},
		suggestionItem: {
			display: 'block',
			minWidth: '180px',
			padding: theme.spacing(1),
			textAlign: 'left',
			textTransform: 'none',
			width: '100%',
		},
		suggestions: {
			background: '#fff',
			marginTop: '12px',
			position: 'absolute',
			zIndex: 100,
		},
	}))()

	return (
		<div className={localClasses.search}>
			{withIcon && (
				<div className={localClasses.searchIcon}>
					<SearchIcon />
				</div>
			)}
			<InputBase
				autoFocus={true}
				classes={{
					root: localClasses.inputRoot,
					input: localClasses.inputInput,
				}}
				inputProps={{ 'aria-label': 'search' }}
				onChange={onChange}
				placeholder='Search…'
				value={value}
			/>
			{suggestions && suggestions.length > 0 && (
				<Paper className={localClasses.suggestions} elevation={7}>
					{suggestions.map((item, index) => (
						<Button
							className={localClasses.suggestionItem}
							key={index}
							onClick={() => onChange({ target: { value: item, search: true } })}
						>
							{item}
						</Button>
					))}
				</Paper>
			)}
		</div>
	)
}

export default SearchBar
