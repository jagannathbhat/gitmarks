import React from 'react'
import BookmarkIcon from '@material-ui/icons/Bookmark'
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder'
import {
	Card,
	CardActionArea,
	CardContent,
	CardMedia,
	IconButton,
	Typography,
} from '@material-ui/core'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'

import { alertAdd } from '../../actions/alertActions'
import { repoAdd, repoRemove } from '../../actions/repoActions'
import { trimString } from '../../actions/utils'
import { useStyles } from '../../theme'

const mapStateToProps = state => ({ repos: state.repos })

const CardComponent = ({
	alertAdd,
	description,
	full_name,
	name,
	repoAdd,
	repoRemove,
	type,
	url_avatar,
	url_repo,
}) => {
	const bookmarkAdd = () => {
		alertAdd('Bookmark Added')
		repoAdd({
			description,
			full_name,
			name,
			url_avatar,
			url_repo,
		})
	}

	const bookmarkRemove = () => {
		alertAdd('Bookmark Removed')
		repoRemove(url_repo)
	}

	const classes = useStyles()

	if (type === 1)
		return (
			<Link
				className={`${classes.card} ${classes.linkNormalize}`}
				title={name}
				to={{
					pathname: '/user-repos',
					state: url_repo,
				}}
			>
				<img alt={name} className={classes.avatarImage} src={url_avatar} />
				<Typography
					component='h2'
					gutterBottom
					style={{ textAlign: 'center' }}
					variant='h5'
				>
					{trimString(name, 19)}
				</Typography>
			</Link>
		)

	return (
		<Card className={classes.card} title={full_name}>
			{window.localStorage.getItem(url_repo) ? (
				<IconButton
					aria-label='remove boomark'
					className={classes.bookmarkIcon}
					onClick={bookmarkRemove}
					title='Remove Boomark'
				>
					<BookmarkIcon />
				</IconButton>
			) : (
				<IconButton
					aria-label='add boomark'
					className={classes.bookmarkIcon}
					onClick={bookmarkAdd}
					title='Add Boomark'
				>
					<BookmarkBorderIcon />
				</IconButton>
			)}
			<a
				className={classes.linkNormalize}
				href={url_repo}
				rel='noopener noreferrer'
				target='_blank'
			>
				<CardActionArea>
					{type === 0 && (
						<CardMedia className={classes.cardImage} image={url_avatar} />
					)}
					{type === 3 && <div style={{ height: '48px' }}></div>}
					<CardContent>
						<Typography component='h2' gutterBottom variant='h5'>
							{trimString(name, 18)}
						</Typography>
						<Typography color='textSecondary' component='p' variant='body2'>
							{trimString(description, 90)}
						</Typography>
					</CardContent>
				</CardActionArea>
			</a>
		</Card>
	)
}

export default withRouter(
	connect(mapStateToProps, { alertAdd, repoAdd, repoRemove })(CardComponent)
)
