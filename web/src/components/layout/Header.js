import React from 'react'
import { AppBar, Toolbar } from '@material-ui/core'

import { useStyles } from '../../theme'

const Header = ({ children }) => {
	const classes = useStyles()
	return (
		<AppBar position='static'>
			<Toolbar className={classes.toolbar}>{children}</Toolbar>
		</AppBar>
	)
}

export default Header
