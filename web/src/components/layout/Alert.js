import React, { useEffect, useState } from 'react'
import CloseIcon from '@material-ui/icons/Close'
import { IconButton, Snackbar } from '@material-ui/core'
import { connect } from 'react-redux'

import { alertRemove } from '../../actions/alertActions'
import store from '../../store'

const mapStateToProps = state => ({ alerts: state.alerts })

const Alert = ({ alertRemove, alerts }) => {
	const [message, setMessage] = useState('')
	const [open, setOpen] = useState(false)

	useEffect(() => {
		store.subscribe(() => {
			setMessage(alerts.current)
			setOpen(alerts.open)
		})
		// eslint-disable-next-line
	}, [])

	return (
		<Snackbar
			action={
				<IconButton
					size='small'
					aria-label='close'
					color='inherit'
					onClick={alertRemove}
				>
					<CloseIcon fontSize='small' />
				</IconButton>
			}
			anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
			autoHideDuration={6000}
			message={message}
			onClose={alertRemove}
			open={open}
		/>
	)
}

export default connect(mapStateToProps, { alertRemove })(Alert)
