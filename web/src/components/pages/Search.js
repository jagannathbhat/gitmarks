import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import BackIcon from '@material-ui/icons/ArrowBack'
import SearchIcon from '@material-ui/icons/Search'
import {
	Button,
	CircularProgress,
	IconButton,
	makeStyles,
	Typography,
} from '@material-ui/core'
import { Pagination } from '@material-ui/lab'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import AppTitle from '../layout/AppTitle'
import CardComponent from '../layout/Card'
import Header from '../layout/Header'
import SearchBar from '../layout/SearchBar'
import { alertAdd } from '../../actions/alertActions'
import {
	recentSearchesAdd,
	recentSearchesGet,
} from '../../actions/recentSearchesActions'
import { useStyles } from '../../theme'

const LIMIT = 12

const API_REPOS = `https://api.github.com/search/repositories?per_page=${LIMIT}&q=`

const API_USERS = `https://api.github.com/search/users?per_page=${LIMIT}&q=`

const getCount = total_count => {
	const div = parseInt(total_count / LIMIT)
	if (div * LIMIT === total_count) return div
	return div + 1
}

const useLocalStyles = makeStyles(theme => ({
	body: { textAlign: 'center' },
	filterButton: {
		border: '2px solid ' + theme.palette.common.white,
		borderRadius: '36px',
		color: theme.palette.common.white,
		cursor: 'pointer',
		fontSize: theme.typography.body2.fontSize,
		marginRight: theme.spacing(2),
		opacity: theme.palette.action.disabledOpacity,
		padding: '4px 12px',
	},
	filterButtonSelected: { opacity: 1 },
	filterWrap: {
		display: 'inline-block',
		[theme.breakpoints.down('sm')]: {
			margin: `${theme.spacing(2)}px 0px`,
			textAlign: 'center',
		},
	},
	loading: {
		margin: theme.spacing(1),
		marginTop: theme.spacing(10),
	},
	paginationWrap: {
		display: 'flex',
		justifyContent: 'flex-end',
		margin: theme.spacing(3),
	},
	recent: {
		margin: theme.spacing(2) + 'px auto',
		maxWidth: '480px',
		textAlign: 'center',
	},
	recentItem: {
		border: '2px solid ' + theme.palette.common.black,
		borderRadius: '60px',
		cursor: 'pointer',
		display: 'inline-block',
		fontSize: theme.typography.body1.fontSize,
		margin: theme.spacing(1),
		padding: '6px 24px',
	},
	recentTitle: {
		marginBottom: theme.spacing(4),
		marginTop: theme.spacing(10),
	},
	searchIcon: { marginLeft: theme.spacing(2) },
}))

const Search = ({ alertAdd }) => {
	const [cards, setCards] = useState([])
	const [count, setCount] = useState(0)
	const [filter, setFilter] = useState(0)
	const [loading, setLoading] = useState(false)
	const [pageNo, setPageNo] = useState(1)
	const [queryText, setQueryText] = useState('')
	const [recentSearches, setRecentSearches] = useState(recentSearchesGet())
	const [results, setResults] = useState([])
	const [searched, setSearched] = useState(false)
	const [suggestions, setSuggestions] = useState([])

	const classes = useStyles()

	const getApiUrl = item => {
		if (!item) item = queryText
		return (filter === 0 ? API_REPOS : API_USERS) + item + '&page=' + pageNo
	}

	const getSearchResults = (item, cards = false) => {
		item = getApiUrl(item)
		setLoading(true)
		Axios.get(item)
			.then(({ data: { items, total_count } }) => {
				const newSuggestions = []
				let newResults = []

				if (filter === 0) {
					newResults = items.map(
						({ description, full_name, name, owner, html_url }) => {
							if (!newSuggestions.includes(name)) newSuggestions.push(name)
							return {
								description,
								full_name,
								name,
								url_avatar: owner.avatar_url,
								url_repo: html_url,
							}
						}
					)
				} else
					newResults = items.map(({ avatar_url, login, repos_url }) => {
						if (!newSuggestions.includes(login)) newSuggestions.push(login)
						return {
							name: login,
							url_avatar: avatar_url,
							url_repo: repos_url,
						}
					})

				if (cards) setCards(newResults)
				else {
					setResults(newResults)
					setSuggestions(newSuggestions)
				}

				setCount(getCount(total_count))
			})
			.catch(error => alertAdd(error.message))
			.finally(() => setLoading(false))
	}

	const localClasses = useLocalStyles()

	const pageChangeHandler = (event, value) => setPageNo(value)

	const recentSelectHandler = item => {
		setQueryText(item)
		getSearchResults(item, true)
	}

	const searchChangeHandler = ({ target: { search, value } }) => {
		setQueryText(value)
		if (value === '') return setSuggestions([])

		getSearchResults(value)

		if (search) {
			submitHandler()
			setSuggestions([])
		}
	}

	const submitHandler = event => {
		if (event) event.preventDefault()
		if (queryText === '') {
			setCards([])
			return setSearched(false)
		}
		if (pageNo === 1) {
			setCards(results)
			setRecentSearches(recentSearchesAdd(queryText))
			return setSearched(true)
		}
		setPageNo(1)
		setSuggestions([])
	}

	const FilterButton = ({ index, label, name }) => (
		<Button
			aria-label={label}
			className={
				filter === index
					? `${localClasses.filterButton} ${localClasses.filterButtonSelected}`
					: localClasses.filterButton
			}
			onClick={() => setFilter(index)}
			title={label}
		>
			{name}
		</Button>
	)

	const LoadingComponent = () => (
		<Typography
			className={localClasses.loading}
			color='textSecondary'
			variant='h2'
		>
			Loading <CircularProgress color='inherit' />
		</Typography>
	)

	const RecentSearchesComponent = () => (
		<div className={localClasses.recent}>
			<Typography
				className={localClasses.recentTitle}
				color='textSecondary'
				variant='h4'
			>
				{recentSearches.length > 0
					? 'Recent Searches'
					: 'Search results appear here'}
			</Typography>
			{recentSearches.map((item, index) => (
				<div
					className={localClasses.recentItem}
					key={index}
					onClick={() => recentSelectHandler(item)}
				>
					{item}
				</div>
			))}
		</div>
	)

	const ResultsComponent = () => (
		<>
			{cards.map((item, index) => (
				<CardComponent key={index} type={filter} {...item} />
			))}
			{count > 0 && (
				<div className={localClasses.paginationWrap}>
					<Pagination
						count={count}
						onChange={pageChangeHandler}
						page={pageNo}
					/>
				</div>
			)}
		</>
	)

	useEffect(() => {
		if (queryText !== '') {
			setCards([])
			setCount(0)
			setPageNo(1)
			setQueryText('')
		}
		// eslint-disable-next-line
	}, [filter])

	useEffect(() => {
		if (queryText !== '') getSearchResults()
		// eslint-disable-next-line
	}, [pageNo])

	return (
		<div className={classes.root}>
			<Header>
				<Link to='/'>
					<IconButton
						aria-label='go back'
						className={classes.primaryText}
						title='Go Back'
					>
						<BackIcon />
					</IconButton>
				</Link>
				<AppTitle />
				<form onSubmit={submitHandler} style={{ display: 'inline-block' }}>
					<SearchBar
						onChange={searchChangeHandler}
						suggestions={suggestions}
						value={queryText}
					/>
					<IconButton
						aria-label='search'
						className={`${classes.primaryText} ${localClasses.searchIcon}`}
						type='submit'
						title='Search'
					>
						<SearchIcon />
					</IconButton>
				</form>
				<div className={localClasses.filterWrap}>
					<span style={{ marginRight: '12px' }}>Filter:</span>
					<FilterButton
						index={0}
						label='Search Repositories Only'
						name='Repositories'
					/>
					<FilterButton index={1} label='Search Users Only' name='Users' />
				</div>
			</Header>
			<div className={localClasses.body}>
				{loading ? (
					<LoadingComponent />
				) : cards.length === 0 ? (
					searched ? (
						<Typography style={{ marginTop: '60px' }} variant='h4'>
							No Results Found
						</Typography>
					) : (
						<RecentSearchesComponent />
					)
				) : (
					<ResultsComponent />
				)}
			</div>
		</div>
	)
}

export default connect(null, { alertAdd })(Search)
