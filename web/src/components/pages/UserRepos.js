import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import BackIcon from '@material-ui/icons/ArrowBack'
import {
	CircularProgress,
	IconButton,
	makeStyles,
	Typography,
} from '@material-ui/core'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import CardComponent from '../layout/Card'
import Header from '../layout/Header'
import { alertAdd } from '../../actions/alertActions'
import { useStyles } from '../../theme'

const useLocalStyles = makeStyles(theme => ({
	body: { textAlign: 'center' },
	filterButton: {
		border: '2px solid ' + theme.palette.common.white,
		borderRadius: '36px',
		color: theme.palette.common.white,
		cursor: 'pointer',
		fontSize: theme.typography.body2.fontSize,
		marginRight: theme.spacing(2),
		opacity: theme.palette.action.disabledOpacity,
		padding: '4px 12px',
	},
	filterButtonSelected: { opacity: 1 },
	headerTitle: {
		fontSize: theme.typography.h5.fontSize,
		marginLeft: theme.spacing(1),
		verticalAlign: 'middle',
	},
	loading: {
		margin: theme.spacing(1),
		marginTop: theme.spacing(10),
	},
	paginationWrap: {
		display: 'flex',
		justifyContent: 'flex-end',
		margin: theme.spacing(3),
	},
	recent: {
		cursor: 'pointer',
		margin: theme.spacing(2) + 'px auto',
		maxWidth: '480px',
		textAlign: 'center',
	},
	recentItem: {
		border: '2px solid ' + theme.palette.common.black,
		borderRadius: '60px',
		display: 'inline-block',
		fontSize: theme.typography.body1.fontSize,
		margin: theme.spacing(1),
		padding: '6px 24px',
	},
	recentTitle: {
		marginBottom: theme.spacing(4),
		marginTop: theme.spacing(10),
	},
	searchIcon: { marginLeft: theme.spacing(2) },
}))

const UserRepos = ({ alertAdd, location }) => {
	const { state: repos_url } = location
	const [cards, setCards] = useState([])
	const [loading, setLoading] = useState(false)

	const classes = useStyles()

	const localClasses = useLocalStyles()

	const LoadingComponent = () => (
		<Typography
			className={localClasses.loading}
			color='textSecondary'
			variant='h2'
		>
			Loading <CircularProgress color='inherit' />
		</Typography>
	)

	const ResultsComponent = () => (
		<>
			{cards.map((item, index) => (
				<CardComponent key={index} type={3} {...item} />
			))}
		</>
	)

	useEffect(() => {
		if (repos_url) {
			setLoading(true)
			Axios.get(repos_url)
				.then(({ data }) => {
					setCards(
						data.map(({ description, full_name, name, owner, html_url }) => ({
							description,
							full_name,
							name,
							url_avatar: owner.avatar_url,
							url_repo: html_url,
						}))
					)
				})
				.catch(error => alertAdd(error.message))
				.finally(() => setLoading(false))
		}
		// eslint-disable-next-line
	}, [])

	return (
		<div className={classes.root}>
			<Header>
				<Link to='/search'>
					<IconButton
						aria-label='go back'
						className={classes.primaryText}
						title='Go Back'
					>
						<BackIcon />
					</IconButton>
				</Link>
				<span className={localClasses.headerTitle}>GitMarks</span>
			</Header>
			<div className={localClasses.body}>
				{loading ? (
					<LoadingComponent />
				) : cards.length === 0 ? (
					<Typography style={{ marginTop: '60px' }} variant='h4'>
						No Results Found
					</Typography>
				) : (
					<ResultsComponent />
				)}
			</div>
		</div>
	)
}

export default connect(null, { alertAdd })(UserRepos)
