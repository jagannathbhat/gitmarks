import React, { useEffect, useState } from 'react'
import AddIcon from '@material-ui/icons/Add'
import { IconButton, makeStyles, Typography } from '@material-ui/core'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import AppTitle from '../layout/AppTitle'
import CardComponent from '../layout/Card'
import Header from '../layout/Header'
import SearchBar from '../layout/SearchBar'
import { repoInit } from '../../actions/repoActions'
import { useStyles } from '../../theme'

const mapStateToProps = state => ({ repos: state.repos })

const useLocalStyles = makeStyles(theme => ({
	addIcon: { marginLeft: theme.spacing(2) },
	body: {
		[theme.breakpoints.down('sm')]: { textAlign: 'center' },
	},
}))

const Home = ({ repoInit, repos }) => {
	const [cards, setCards] = useState([])
	const [queryText, setQueryText] = useState('')

	const classes = useStyles()

	const localClasses = useLocalStyles()

	const searchChangeHandler = ({ target: { value } }) => {
		setQueryText(value)
		setCards(
			repos.filter(item =>
				JSON.stringify(item).toLowerCase().includes(value.toLowerCase())
			)
		)
	}

	useEffect(() => {
		setCards(repos)
		// eslint-disable-next-line
	}, [repos])

	useEffect(() => {
		repoInit()
		// eslint-disable-next-line
	}, [])

	return (
		<div className={classes.root}>
			<Header>
				<AppTitle />
				<SearchBar
					onChange={searchChangeHandler}
					value={queryText}
					withIcon={true}
				/>
				<Link className={localClasses.addIcon} to='/search'>
					<IconButton
						aria-label='add repository'
						className={classes.primaryText}
						title='Add Repository'
					>
						<AddIcon />
					</IconButton>
				</Link>
			</Header>
			{cards.length === 0 ? (
				<Typography
					color='textSecondary'
					style={{ margin: '12px', marginTop: '60px', textAlign: 'center' }}
					variant='h4'
				>
					No Repos Bookmarked
				</Typography>
			) : (
				<div className={localClasses.body}>
					{cards.map((item, index) => (
						<CardComponent key={index} type={0} {...item} />
					))}
				</div>
			)}
		</div>
	)
}

export default connect(mapStateToProps, { repoInit })(Home)
