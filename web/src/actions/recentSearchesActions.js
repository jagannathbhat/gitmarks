export const recentSearchesAdd = newSearch => {
	let recentSearches = window.localStorage.getItem('recent_searches')
	recentSearches = recentSearches ? JSON.parse(recentSearches) : []

	if (recentSearches.includes(newSearch) || newSearch === '')
		return recentSearches

	recentSearches.push(newSearch)
	if (recentSearches.length > 10) recentSearches = recentSearches.slice(1, 11)
	window.localStorage.setItem('recent_searches', JSON.stringify(recentSearches))
	return recentSearches
}

export const recentSearchesGet = () => {
	let recentSearches = window.localStorage.getItem('recent_searches')
	return recentSearches ? JSON.parse(recentSearches) : []
}
