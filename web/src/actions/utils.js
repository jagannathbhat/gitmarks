export const trimString = (string, num) => {
	if (!num) return string
	if (!string) return null
	if (string.length > num) return string.slice(0, num - 3) + '...'
	return string
}
