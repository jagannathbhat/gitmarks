import { REPO_ADD, REPO_INIT, REPO_REMOVE } from '../types'

export const repoAdd = repo => async dispatch => {
	dispatch({ type: REPO_ADD, payload: repo })
	window.localStorage.setItem(repo.url_repo, JSON.stringify(repo))
}

export const repoRemove = url_repo => async dispatch => {
	dispatch({ type: REPO_REMOVE, payload: url_repo })
	window.localStorage.removeItem(url_repo)
}

export const repoInit = () => async dispatch => {
	const no_of_repos = window.localStorage.length
	const repos = []

	for (let i = 0; i < no_of_repos; i++) {
		const key = window.localStorage.key(i)
		if (key === 'recent_searches') continue
		const repo = window.localStorage.getItem(key)

		if (repo) repos.push(JSON.parse(repo))
	}

	dispatch({ type: REPO_INIT, payload: repos })
}
