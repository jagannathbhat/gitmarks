import { ALERT_ADD, ALERT_NEXT, ALERT_REMOVE } from '../types'

export const alertAdd = msg => async dispatch => {
	dispatch({ type: ALERT_ADD, payload: msg })
}

export const alertRemove = () => async dispatch => {
	dispatch({ type: ALERT_REMOVE })
	setTimeout(() => {
		dispatch({ type: ALERT_NEXT })
	}, 1000)
}
