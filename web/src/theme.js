import { fade, makeStyles } from '@material-ui/core'

export const useStyles = makeStyles(theme => ({
	avatarImage: {
		borderRadius: '100%',
		height: '240px',
		width: '240px',
	},
	bookmarkIcon: {
		background: fade(theme.palette.common.black, 0.5),
		color: theme.palette.common.white,
		margin: theme.spacing(1),
		position: 'absolute',
		zIndex: 9,
		'&:hover': {
			background: fade(theme.palette.common.black, 0.5),
		},
	},
	card: {
		cursor: 'pointer',
		display: 'inline-block',
		margin: theme.spacing(3),
		textAlign: 'left',
		verticalAlign: 'top',
		width: '240px',
	},
	cardImage: { height: '240px' },
	centerizer: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		textAlign: 'center',
	},
	headerTitle: {
		fontSize: theme.typography.h5.fontSize,
		marginLeft: theme.spacing(3),
		[theme.breakpoints.down('sm')]: { display: 'none' },
	},
	linkNormalize: { color: 'inherit', textDecoration: 'inherit' },
	primaryText: { color: '#fafafa' },
	root: {
		borderRadius: '0px',
		display: 'flex',
		flex: 1,
		flexDirection: 'column',
	},
	toolbar: {
		minHeight: 'auto',
		margin: `${theme.spacing(1)}px 0px`,
		[theme.breakpoints.down('sm')]: {
			display: 'block',
		},
	},
	whiteSpace: {
		[theme.breakpoints.down('sm')]: { display: 'inline-block' },
		[theme.breakpoints.up('sm')]: { flexGrow: 1 },
	},
}))
