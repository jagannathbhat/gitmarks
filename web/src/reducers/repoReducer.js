import { REPO_ADD, REPO_INIT, REPO_REMOVE } from '../types'

const initialState = []

export default (state = initialState, action) => {
	switch (action.type) {
		case REPO_ADD:
			return [...initialState, action.payload]
		case REPO_INIT:
			return action.payload
		case REPO_REMOVE:
			return state.filter(({ url_repo }) => url_repo !== action.payload)
		default:
			return state
	}
}
