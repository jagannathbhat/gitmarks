import { ALERT_ADD, ALERT_NEXT, ALERT_REMOVE } from '../types'

const initialState = {
	current: '',
	open: false,
}

const queue = []

export default (state = initialState, action) => {
	switch (action.type) {
		case ALERT_ADD:
			if (state.current === '' && queue.length === 0) {
				state.current = action.payload
				state.open = true
			} else queue.push(action.payload)
			return state

		case ALERT_NEXT:
			if (queue.length > 0) {
				state.current = queue.pop()
				state.open = true
			}
			return state

		case ALERT_REMOVE:
			if (state.current !== '') {
				state.current = ''
				state.open = false
			}
			return state

		default:
			return state
	}
}
