import { combineReducers } from 'redux'

import alertReducer from './alertReducer'
import repoReducer from './repoReducer'

export default combineReducers({
	alerts: alertReducer,
	repos: repoReducer,
})
