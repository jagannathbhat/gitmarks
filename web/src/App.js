import React from 'react'
import { CssBaseline } from '@material-ui/core'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import store from './store'
import Home from './components/pages/Home'
import Search from './components/pages/Search'
import UserRepos from './components/pages/UserRepos'
import Alert from './components/layout/Alert'
// import { getMode, setMode } from './actions/darkModeActions'

function App() {
	// const [darkMode, setDarkMode] = useState(getMode())

	// const toggleDarkMode = () => {
	// 	setDarkMode(!darkMode)
	// 	setMode(!darkMode)
	// }

	// const theme = React.useMemo(
	// 	() =>
	// 		createMuiTheme({
	// 			palette: {
	// 				type: darkMode ? 'dark' : 'light',
	// 			},
	// 		}),
	// 	[darkMode]
	// )

	return (
		<Provider store={store}>
			{/* <ThemeProvider theme={theme}> */}
			{/* </ThemeProvider> */}
			<CssBaseline />
			<Router basename={process.env.PUBLIC_URL}>
				<Switch>
					<Route component={Home} exact path='/' />
					<Route component={Search} exact path='/search' />
					<Route component={UserRepos} exact path='/user-repos' />
				</Switch>
			</Router>
			<Alert />
		</Provider>
	)
}

export default App
